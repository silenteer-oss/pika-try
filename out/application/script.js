import React from "/web_modules/react.js";
import ReactDOM from "/web_modules/react-dom.js";

function HelloWorld() {
  return React.createElement("div", null, "HelloWorld");
}

ReactDOM.render(React.createElement(HelloWorld, null), document.getElementById("app"));